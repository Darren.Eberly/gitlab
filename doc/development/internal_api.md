---
redirect_to: 'internal_api/index.md'
remove_date: '2022-02-09'
---

This document was moved to [another location](internal_api/index.md).

<!-- This redirect file can be deleted after <YYYY-MM-DD>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
